package gui;

public interface BasicGuiInterface {

  /**
   * Il metodo carica la GUI con i rispettivi dati.
   */
  void run();

  /**
   * Il metodo permette di ricaricare la grafica della GUI.
   */
  void reload();

  /**
   * Il metodo permette la chiusura del frame indicato.
   */
  void dispose();

}
