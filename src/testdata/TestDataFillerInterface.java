package testdata;

public interface TestDataFillerInterface {

  /**
   * Il metodo riempie le diverse classi con dati d'esempio.
   */
  void run();

}
